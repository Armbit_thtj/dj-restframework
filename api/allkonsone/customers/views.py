# REST FRAMEWORK & LIB
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.http import HttpResponse
import json
from rest_framework.parsers import JSONParser

# CONSTANTS 
from constant.config import (HEADER)

# MODELS | SERIALIZERS
from allkonsone.models import (customers)
from allkonsone.customers.serializers import (customerSerializer)

@api_view(["GET"])
def findAll(request):
    try:
        customers_data = customers.objects.all() 
        customers_data = customerSerializer(customers_data,many=True)
        return Response({"message":"ok","data":customers_data.data},status=status.HTTP_200_OK,content_type='application/json')
    except:
        return HttpResponse(json.dumps({"status":status.HTTP_500_INTERNAL_SERVER_ERROR,"message":"INTERNAL SERVER ERROR"}),HEADER)


@api_view(["POST"])
def create(request):
    try:
        customer = JSONParser().parse(request)
        serializer = customerSerializer(data=customer)
        if serializer.is_valid():
            serializer.save()
        else:
            return HttpResponse(json.dumps({"status":status.HTTP_400_BAD_REQUEST,"message":serializer.errors}),HEADER)

        return HttpResponse(json.dumps({"status":status.HTTP_200_OK,"message":"created customer from dj"}),HEADER)
    except Exception as e:
        print("create customer error : ",e)
        return HttpResponse(json.dumps({"status":status.HTTP_500_INTERNAL_SERVER_ERROR,"message":"INTERNAL SERVER ERROR"}),HEADER)
