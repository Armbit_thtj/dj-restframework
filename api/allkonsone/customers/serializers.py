from rest_framework import serializers
from allkonsone.models import customers

class customerSerializer(serializers.ModelSerializer):
    class Meta:
        model = customers
        fields = "__all__"