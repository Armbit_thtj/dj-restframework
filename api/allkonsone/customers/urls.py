from django.urls import path
from allkonsone.customers.views import (findAll,create)
app_name = 'api'

urlpatterns = [
    path('customer/',findAll),
    path('customer',create)
]