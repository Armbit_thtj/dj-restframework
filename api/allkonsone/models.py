from django.db import models

# Create your models here.

class customers(models.Model):
    firstname = models.CharField(max_length=255)
    lastname = models.CharField(max_length=255)
    phone = models.CharField(max_length=10)
    email = models.CharField(max_length=255)
    created_on = models.DateField(auto_now_add=True)
    updated_on = models.DateField(auto_now=True)
